# Les parties prenantes du modèle Scrum

Les relations du modèle scrum avec le monde extérieur

Les parties prenantes (*stakeholders* en anglais): personnes en dehors de l'équipe scrum ayant des connexions plus ou moins fortes avec le projet à l'exemple de:

- sponsor impactefinancièrement
- L'utilisateur final concerné directement par les usages

**Résumé**:

- Vos parties prenantes ont des **connexions plus ou moins fortes** avec le projet.
 

- Invitez des **utilisateurs, des experts et des spécialistes** pour aider l'équipe Scrum.
 

- En qualité de scrum master, vous **interagissez** régulièrement avec les parties prenantes et vous **légitimez** leur participation au projet.
  

- Installez l'équipe dans une **salle dédiée** afin d'optimiser votre management visuel.
