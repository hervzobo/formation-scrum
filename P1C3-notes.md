# L'équipe projet

## Rôle d'une équipe de développement

- En charge des opérations du projet
- Livre au client à intervalles réguliers des fonctionnalités complètes
- Les membres sont multi-compétences
- Pluridisciplinaires
- Autonomes.

**Notes**:
- constitution de l'équipe 3 à 9 personnes
- Encourager le partage de responsabilité
- Bon ou mauvais, ne jamais attribuer les résultats à une seule personne.

**L'équipe**:
- ne peut pas être multi-produits
- détermine seul ses choix de solution
- est indissociable et les membres sont considérés au même niveau
- estime sa charge de travail et détermine sa capacité à réaliser une tâche
- organise et réalise les tâches du projet en respectant le modèle Scrum
- est responsable du projet et de l'atteinte des objectifs
- participe avec le product owner à toutes les cérémonies Scrum.

**Note**: Toutes les décisions au sein de l'équipe sont prises unanimement.

## Mission de l'équipe de développement

Associer Scrum à un *ensemble de pratiques agiles* complémentaires, car ce dernier ne décrit pas en détail tous les aspects de la gestion de projet.

**Ensemble de pratiques Agiles afin de garantir la cohésion et la performance de l'équipe**:

1. Le planning poker: 
- Estimation de la complexité de chaque *user story* avec le product owner
- Possibilité d'utilisation d'un jeu de carte représentant différentes valeurs pour l'estimation en points des user stories.
- Découvrir simultanément les cartes des participants et lancer un débat à partir des valeurs les plus extrêmes.
- Et si nécessaire repeter le jeu jusqu'à l'obtention du consensus.

2. Le tableau **kanban**
- représentation de chaque user story sur les affichettes.
- repartir sur un tableau divisé en 3 colonnes: *A faire*, *En cours*, *Terminé*
- Actualisation lors des mêlées quotidiennes à fin de visualiser la progression de l'équipe.
- Remettre le tableau à zéro en début d'itération afin d'ajouter de nouvelles user stories.

3. L'attribution des tâches
- Lister les tâches de chaque user story pour développer les fonctionnalités du produit
- Pas de désignation d'un chef chargé d'assigner les tâches
- Repartir les user stories sur la base du volontariat.
