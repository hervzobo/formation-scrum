# Négociez avec le client

## Le product owner
Personne qui représente le client au sein de l'équipe scrum

### Rôle du product owner

La participation du client dans le projet est incontournable, cependant, ce dernier peut rencontrer plusieurs difficultés:
- Manque de disponibilité
- Manque d'expérience en gestion de projet agile
- Manque d'informations sur les utilisateurs...
D'où le rôle du product owner qui est **garant des intérêts du client** et le seul pouvant modifier ou compléter la liste des fonctionnalités.
**Note**: le product owner n'est pas votre supérieur hiérarchique. Il doit être très disponible à répondre aux questions et donner son avis.

### Mission du product owner
- *L'expréssion des besoins* (user stories)
- *La priorisation des besoins*
- *la validation des résultats*

*Un user story* est une phrase simple et compréhensible qui décrit une fonctionnalité ou un service.
L'ensemble des *user stories* constitue le **carnet de produit** (product backlog).

Selon le guide officiel de scrum: Le *product backlog* est une liste ordonnée de tout ce qui pourrait être requis dans le produit et est l'unique source des besoins pour tous les changements à effectuer sur le produit.