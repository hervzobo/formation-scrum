# Le Scrum master

## Le rôle du Scrum master

- Responsable de l'adhésion, la cohésion, la compréhension et la mise en oeuvre du modèle Scrum.
- Au service de l'équipe développement, du product owner et du projet.
- Favoriser les bonnes intéractions afin d'optimiser la valeur finale du produit
- Il s'agit d'un leader au service de l'équipe.
- aplanit les difficultés qui se présentent à l'équipe pour fluidifier la production
- Améliore la communication au sein et à l'extérieur de l'équipe
- Suit et communique les indicateurs pendant la gestion du projet
- Explique et fait respecter les règles, les usages et les valeurs Scrum
- coache et anime les atéliers autour de l'amélioration continue des pratiques agiles
- encourage l'esprit d'équipe et maintient la motivation
- Facilite l'intégration du modèle scrum dans l'entreprise.

**Notes**: 
- Le scrum master n'est pas un manager pour prendre les décisions à la place de l'équipe
- Pas de mission technique ou opérationnelle
- N'intervient pas directement dans le développement du projet
- Ne définit pas le budget.

## La mission du Scrum master
 Il est au service des acteurs du projet pour remplir une mission de **facilitation**

 Les qualités d'un facilitateur:
 - l'empathie
 - la bienveillance
 - La rigueur
 - l'humour...

 ### Création des conditions de travail satisfaisantes:
 - *Valorisez la participation* des acteurs du projet (sans les freiner)
 - *Assurez l'équilibre* au sein de l'équipe (sans jeu d'influence)
 - *Présentez des textes et des illustrations agréables* (sans superficialité)
 - *Garantissez le respect* des uns des autres (sans autoritarisme)
 - *Simplifiez la complexité* des organisations (sans les vulgariser à l'extrême)

 ### Les actions mener par le Scrum master
 - Accompagnez les salariés ou les adhérents dans l'adoption du modèle Scrum
 - Planifiez la mise en oeuvre progressive du modèle scrum
 - Aidez toutes les parties prenantes à comprendre l'approche empirique du modèle Scrum.
 - Provoquez les changements qui augmentent l'efficacité du modèle Scrum
 - Coopérez avec d'autres Scrum masters pour améliorer l'utilisation du modèle Scrum