# Les piliers Scrum

## Les fondements du modèle scrum
un cadre reposant sur 3 piliers:
- **Transparence**: gararntir que les informations nécessaires à la compréhension du projet sont comuniquées aux différents membres du projet.
Leviers efficaces de valorisation de la **transparence**:
  - *Un langage commun* partagé par tous les membres de l'équipe projet;
  -*Un management visuel* qui propose des informations libres d'accès, pertinentes et utiles.
  **Note**: Pour un Scrum master, la **transparence** favorise le **contrôle du modèle scrum**.
  - *Une communication bienveillante et conviviale* entre tous les acteurs
- L'**Inspection**: vérification à intervalle régulier du respect des limites acceptables par rapport à la demande du client.
- L'**adaptation**: encourager la correction des dérives constatées et proposition des changements appropriés pour mieux répondre aux objectifs de la gestion du projet.

**Note**: le concepteur de **Scrum** Ken Schwaber, l'un des 17 experts à l'origine du **manifeste agile**.
[Cycle de vie d'un projet scrum](https://user.oc-static.com/upload/2017/10/04/15071250336374_scrum-diagram.png)
 ## Les 4 principes et 12 valeurs du manifeste agile
 ### Les 4 principes
 1. *Les individus et leurs interactions plus que les processus et les outils*
 2. *Des logiciels opérationnels plus qu'une documentation exhaustive*
 3. *La collaboration avec les clients plus qu'une négociation contractuelle*
 4. *L'adaptation aux changements plus que le suivi d'un plan*

 ### Les 12 valeurs
 1. *Satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée*
 2. *Accueillir favorablement les demandes de changement même tard dans le développement du produit*
 3. *Livrer le plus souvent possible un logiciel opérationnel avec des cycles de quelques semaines, en privilégiant les délais les plus courts*
 4. *Assurez une coopération permanente entre les parties prenantes et l'équipe produit*
 5. *Réalisez les projets avec des personnes motivées. Fournissez-leur l'environnement et le soutien dont ils ont besoin et faites-leur confiance pour atteindre les objectifs fixés*
 6. *La méthode la plus simple et la plus efficace pour transmettre de l'information est le dialogue en face à face*
 7. *Un produit opérationnel est la principale mesure d'avancement*
 8. *Faire avancer le projet à un rythme soutenable et constant*
 9. *Portez une attention continue à l'excellence technique et à la conception*
 10. *La simplicité- c'est-à-dire l'art de minimiser la quantité de travail inutile est essentielle*
 11. *Les meilleurs architectures, spécifications et conceptions émergent des équipes auto-organisés*
 12. *Réfléchir aux moyens de dévenir plus efficace à intervalles réguliers*

 ## Les caractéristiques du modèle scrum
Le modèle Scrum est un **schéma d'organisation** *empirique*, *holistique*, *itératif*, *incrémental* et *agile*.
- *Une approche empirique*: inspection quotidiennede l'état du projet pour orienter les décisions.
- *Un cadre de travail holistique*: Toujours considérer que la valeur totale du produit ou du service sera supérieure à la somme des décisions.
- *Une méthode itérative*: découpage du projet en cycles identiques ou itérations limitant ainsi les risques d'erreurs.
- *Un développement incrémental*: La partie du projet déjà réalisée doit être utilisable et livrable au client.
- *Une pratique agile*: impliquer le client et les utilisateurs dans la gestion de projet. Choisir des méthodes pragmatiques et adaptatives pour être plus réactif aux demandes.

**Note**: Scrum est un modèle de gestion pour des projets très imprévisibles pas une méthodologie à proprement parler.

Selon le manuel Scrum officiel: *Scrum est un cadre de travail permettant de répondre à des problèmes complexes et changeants, tout en livrant de manière productive et créative des produits de la plus grande valeur possible*

**Résumé**:

- Respectez les 3 piliers du modèle Scrum : la **transparence**, l'**inspection** et l'**adaptation**.
 

En qualité de scrum master, vous valorisez notamment le pilier de la transparence :

*Un langage commun*

*Un management visuel*

*Une communication conviviale*
 

Vous appliquez le modèle Scrum aussi bien dans une agence pour vos clients, que dans une entreprise pour vos projets internes.
 

Scrum est un modèle complet pour gérer des projets imprévisibles et complexes : il est à la fois *empirique*, *holistique*, *itératif*, *incrémental* et *agile*.